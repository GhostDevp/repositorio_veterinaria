-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-05-2021 a las 18:36:41
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_veterinaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `idCita` int(11) NOT NULL,
  `idMascota` int(11) NOT NULL,
  `ciFecha` date DEFAULT NULL,
  `ciEstado` enum('Solicitada','Atendida','Cancelada','Asignada') NOT NULL,
  `idHora` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`idCita`, `idMascota`, `ciFecha`, `ciEstado`, `idHora`) VALUES
(1, 1, '2021-05-21', 'Atendida', 1),
(2, 6, '2021-05-21', 'Atendida', 7),
(3, 7, '2021-05-21', 'Cancelada', 4),
(4, 2, '2021-05-21', 'Atendida', 2),
(5, 3, '2021-05-21', 'Atendida', 6),
(6, 4, '2021-05-21', 'Atendida', 3),
(7, 5, '2021-05-21', 'Solicitada', 5),
(8, 1, '2021-05-21', 'Atendida', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citadetalle`
--

CREATE TABLE `citadetalle` (
  `idCitaDetalle` int(11) NOT NULL,
  `idCita` int(11) NOT NULL,
  `idDetalle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `citadetalle`
--

INSERT INTO `citadetalle` (`idCitaDetalle`, `idCita`, `idDetalle`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 5, 3),
(6, 6, 6),
(7, 8, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citaempleado`
--

CREATE TABLE `citaempleado` (
  `idCitaEmpleado` int(11) NOT NULL,
  `idCita` int(11) NOT NULL,
  `idEmpleado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `citaempleado`
--

INSERT INTO `citaempleado` (`idCitaEmpleado`, `idCita`, `idEmpleado`) VALUES
(1, 1, 1),
(11, 1, 1),
(12, 1, 1),
(2, 2, 1),
(10, 3, 1),
(4, 4, 2),
(3, 5, 1),
(9, 6, 3),
(13, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citaservicio`
--

CREATE TABLE `citaservicio` (
  `idCitaServicio` int(11) NOT NULL,
  `idCita` int(11) NOT NULL,
  `idServicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `citaservicio`
--

INSERT INTO `citaservicio` (`idCitaServicio`, `idCita`, `idServicio`) VALUES
(1, 1, 1),
(2, 2, 4),
(3, 3, 2),
(4, 4, 5),
(5, 5, 1),
(6, 6, 4),
(7, 7, 3),
(8, 8, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE `detalle` (
  `idDetalle` int(11) NOT NULL,
  `deDescripcion` varchar(500) NOT NULL COMMENT 'descripción de la cita'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`idDetalle`, `deDescripcion`) VALUES
(1, 'Se le suministró las vacunas faltantes y se le asigno un Spray para controlar las pulgas de la mascota. '),
(2, 'Se realizo procedimiento quirúrgico para suturar herida en la parte frontal del cráneo.'),
(3, 'Se observa que la mascota no se esta alimentando bien, debido a esto se le receta un purgante.'),
(6, 'Se realiza procedimiento para extraer un objeto extraño del estomago de la mascota.'),
(7, 'Se le aplicaron todas las vacunas.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `empFechaIngreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `idPersona`, `empFechaIngreso`) VALUES
(1, 2, '2020-05-20'),
(2, 3, '2021-01-01'),
(3, 4, '2020-09-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idFactura` int(11) NOT NULL,
  `idEmpleado` int(11) NOT NULL,
  `facFecha` date NOT NULL,
  `facTotal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idFactura`, `idEmpleado`, `facFecha`, `facTotal`) VALUES
(1, 1, '2021-05-19', 126500),
(2, 1, '2021-05-19', 95000),
(3, 1, '2021-05-19', 45000),
(5, 3, '2021-05-19', 95000),
(6, 1, '2021-05-19', 120000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturaproducto`
--

CREATE TABLE `facturaproducto` (
  `idFacturaProducto` int(11) NOT NULL,
  `idFactura` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `facturaproducto`
--

INSERT INTO `facturaproducto` (`idFacturaProducto`, `idFactura`, `idProducto`) VALUES
(1, 1, 3),
(2, 1, 4),
(3, 1, 1),
(4, 2, 8),
(5, 3, 2),
(7, 5, 8),
(8, 6, 3),
(9, 6, 4),
(10, 6, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horasdisponible`
--

CREATE TABLE `horasdisponible` (
  `idHora` int(11) NOT NULL,
  `hoHora` time NOT NULL,
  `hoTipo` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `horasdisponible`
--

INSERT INTO `horasdisponible` (`idHora`, `hoHora`, `hoTipo`) VALUES
(1, '08:00:00', 'am'),
(2, '08:30:00', 'am'),
(3, '09:00:00', 'am'),
(4, '09:30:00', 'am'),
(5, '10:00:00', 'am'),
(6, '10:30:00', 'am'),
(7, '11:00:00', 'am'),
(8, '11:30:00', 'am'),
(9, '02:00:00', 'pm'),
(10, '02:30:00', 'pm'),
(11, '03:00:00', 'pm'),
(12, '03:30:00', 'pm'),
(13, '04:00:00', 'pm'),
(14, '04:30:00', 'pm'),
(15, '05:00:00', 'pm');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascota`
--

CREATE TABLE `mascota` (
  `idMascota` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `idTipoMascota` int(11) NOT NULL,
  `masNombre` varchar(20) NOT NULL,
  `masFechaNacimiento` date NOT NULL,
  `masEdad` int(11) NOT NULL,
  `masEstado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mascota`
--

INSERT INTO `mascota` (`idMascota`, `idPersona`, `idTipoMascota`, `masNombre`, `masFechaNacimiento`, `masEdad`, `masEstado`) VALUES
(1, 5, 2, 'ZEUS', '2016-02-12', 5, 1),
(2, 6, 1, 'TOMAS', '2020-01-24', 1, 0),
(3, 7, 3, 'REBECA', '2018-05-20', 3, 1),
(4, 8, 4, 'KIPLING', '2017-07-08', 4, 1),
(5, 9, 5, 'SIRIO', '2019-04-13', 2, 0),
(6, 5, 4, 'PACO', '2019-08-31', 2, 1),
(7, 6, 2, 'TAIZON', '2018-05-20', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idPersona` int(11) NOT NULL,
  `perIdentificacion` int(11) NOT NULL,
  `perNombre` varchar(15) NOT NULL,
  `perApellido` varchar(45) NOT NULL,
  `perTelefono` varchar(45) NOT NULL,
  `perCorreo` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idPersona`, `perIdentificacion`, `perNombre`, `perApellido`, `perTelefono`, `perCorreo`) VALUES
(1, 1003807116, 'JEFFERSON', 'CARDENAS', '3115845083', 'cardenasjefferson629@gmail.com'),
(2, 1003805501, 'OSCAR', 'ASTUDILLO', '3232516992', 'oskarestiven@gmail.com'),
(3, 1003828436, 'JULIAN', 'SILVA', '3136954685', 'silvajulian012@gmail.com'),
(4, 1003498659, 'JOSE', 'VEGA', '3205841463', 'vegajose@gmail.com'),
(5, 1003806728, 'SELENA', 'RIVERA', '3142402522', 'selequi1231@gmail.com'),
(6, 1003894715, 'FAIBER', 'LARA', '3209814616', 'fadalasu2105@gmail.com'),
(7, 1039596374, 'AGUIRRE', 'MESA', '3154987512', 'mesaaguirre12@gmail.com'),
(8, 1036663164, 'CARLOS', 'GARCIA', '3126985415', 'carlosgarcias321@gmail.com'),
(9, 1036634302, 'PABLO', 'CIFUENTES', '3548756485', 'cifuentespablo213@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL,
  `proNombre` varchar(45) NOT NULL,
  `proPrecio` int(11) NOT NULL,
  `proUnidadMedida` enum('ml','g','mg','UI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `proNombre`, `proPrecio`, `proUnidadMedida`) VALUES
(1, 'Spray Antipulgas', 16500, 'ml'),
(2, 'Purgante', 15000, 'UI'),
(3, 'Vacuna Rabia', 40000, 'ml'),
(4, 'Vacuna Pentavalente', 40000, 'ml'),
(5, 'Bacterina Toxoide', 73500, 'g'),
(7, 'Vacuna Hepatitis', 30000, 'ml'),
(8, 'Zoletil', 15000, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL,
  `rolNombre` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idRol`, `rolNombre`) VALUES
(1, 'Administrador'),
(2, 'Empleado'),
(3, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `idServicio` int(11) NOT NULL,
  `serTipo` varchar(50) NOT NULL,
  `serDescripcion` varchar(1000) NOT NULL,
  `serPrecio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`idServicio`, `serTipo`, `serDescripcion`, `serPrecio`) VALUES
(1, 'Consulta General', 'A su mascota se le realizara una revisión general para evaluar el estado en que se encuentra.', 30000),
(2, 'Belleza', 'A su mascota se le realizara todo lo referente a su apariencia física. ', 20000),
(3, 'Vacunacion', 'Se revisara con que vacunas cuenta su mascota y se le aplicaran las faltantes.', 10000),
(4, 'Cirugía ', 'Procedimiento operatorio, ya sea para determinar la presencia de una enfermedad o reparar una parte del cuerpo.', 80000),
(5, 'Hospitalización ', 'Se le dará una estadía a la mascota con todas las observaciones pertinentes.', 100000),
(6, 'Anestesia General', 'Se le colocara un medicamento que impedirá que su mascota sienta dolor al realizar distintos procedimientos.', 15000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomascota`
--

CREATE TABLE `tipomascota` (
  `idTipoMascota` int(11) NOT NULL,
  `tipoCategoria` enum('Canino','Felino','Ave','Reptil','Otro') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipomascota`
--

INSERT INTO `tipomascota` (`idTipoMascota`, `tipoCategoria`) VALUES
(1, 'Felino'),
(2, 'Canino'),
(3, 'Ave'),
(4, 'Reptil'),
(5, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `idPersona` int(11) NOT NULL,
  `usuLogin` varchar(50) NOT NULL,
  `usuPassword` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `idPersona`, `usuLogin`, `usuPassword`) VALUES
(1, 1, 'cardenasjefferson629@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(2, 2, 'oskarestiven@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(3, 3, 'silvajulian012@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(4, 4, 'vegajose@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(5, 5, 'selequi1231@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(6, 6, 'fadalasu2105@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(7, 7, 'mesaaguirre12@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(8, 8, 'carlosgarcias321@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(9, 9, 'cifuentespablo213@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariorol`
--

CREATE TABLE `usuariorol` (
  `idUsuarioRol` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idRol` int(11) NOT NULL,
  `usuEstado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuariorol`
--

INSERT INTO `usuariorol` (`idUsuarioRol`, `idUsuario`, `idRol`, `usuEstado`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(3, 3, 2, 1),
(4, 4, 2, 1),
(5, 5, 3, 1),
(6, 6, 3, 1),
(7, 7, 3, 1),
(8, 8, 3, 1),
(9, 9, 3, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`idCita`),
  ADD KEY `fk_citas_mascotas1_idx` (`idMascota`),
  ADD KEY `idHora` (`idHora`);

--
-- Indices de la tabla `citadetalle`
--
ALTER TABLE `citadetalle`
  ADD PRIMARY KEY (`idCitaDetalle`),
  ADD KEY `fk_cita_citadetalle` (`idCita`),
  ADD KEY `fk_detalle_citadetalle` (`idDetalle`);

--
-- Indices de la tabla `citaempleado`
--
ALTER TABLE `citaempleado`
  ADD PRIMARY KEY (`idCitaEmpleado`),
  ADD KEY `idCita` (`idCita`,`idEmpleado`),
  ADD KEY `idEmpleado` (`idEmpleado`);

--
-- Indices de la tabla `citaservicio`
--
ALTER TABLE `citaservicio`
  ADD PRIMARY KEY (`idCitaServicio`),
  ADD KEY `idServicio` (`idServicio`),
  ADD KEY `idCita` (`idCita`);

--
-- Indices de la tabla `detalle`
--
ALTER TABLE `detalle`
  ADD PRIMARY KEY (`idDetalle`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`),
  ADD KEY `fk_empleado_persona1_idx` (`idPersona`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `fk_factura_empleado1_idx` (`idEmpleado`);

--
-- Indices de la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  ADD PRIMARY KEY (`idFacturaProducto`),
  ADD KEY `fk_factura_has_producto_producto1_idx` (`idProducto`),
  ADD KEY `fk_factura_has_producto_factura1_idx` (`idFactura`);

--
-- Indices de la tabla `horasdisponible`
--
ALTER TABLE `horasdisponible`
  ADD PRIMARY KEY (`idHora`);

--
-- Indices de la tabla `mascota`
--
ALTER TABLE `mascota`
  ADD PRIMARY KEY (`idMascota`),
  ADD KEY `idTipoMascota` (`idTipoMascota`),
  ADD KEY `idPersona` (`idPersona`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD UNIQUE KEY `perIdentificacion_UNIQUE` (`perIdentificacion`),
  ADD UNIQUE KEY `perCorreo` (`perCorreo`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`idServicio`),
  ADD UNIQUE KEY `serTipo` (`serTipo`),
  ADD KEY `fk_servicio_tiposervicio` (`serTipo`);

--
-- Indices de la tabla `tipomascota`
--
ALTER TABLE `tipomascota`
  ADD PRIMARY KEY (`idTipoMascota`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD UNIQUE KEY `usuLogin_UNIQUE` (`usuLogin`),
  ADD KEY `fk_usuario_persona1_idx` (`idPersona`);

--
-- Indices de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD PRIMARY KEY (`idUsuarioRol`),
  ADD KEY `idUsuario` (`idUsuario`,`idRol`),
  ADD KEY `idRol` (`idRol`),
  ADD KEY `idUsuario_2` (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `idCita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `citadetalle`
--
ALTER TABLE `citadetalle`
  MODIFY `idCitaDetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `citaempleado`
--
ALTER TABLE `citaempleado`
  MODIFY `idCitaEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `citaservicio`
--
ALTER TABLE `citaservicio`
  MODIFY `idCitaServicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `detalle`
--
ALTER TABLE `detalle`
  MODIFY `idDetalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idFactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  MODIFY `idFacturaProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `horasdisponible`
--
ALTER TABLE `horasdisponible`
  MODIFY `idHora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `mascota`
--
ALTER TABLE `mascota`
  MODIFY `idMascota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `idServicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipomascota`
--
ALTER TABLE `tipomascota`
  MODIFY `idTipoMascota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  MODIFY `idUsuarioRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`idHora`) REFERENCES `horasdisponible` (`idHora`),
  ADD CONSTRAINT `fk_citas_mascotas1` FOREIGN KEY (`idMascota`) REFERENCES `mascota` (`idMascota`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `citadetalle`
--
ALTER TABLE `citadetalle`
  ADD CONSTRAINT `fk_cita_citadetalle` FOREIGN KEY (`idCita`) REFERENCES `cita` (`idCita`),
  ADD CONSTRAINT `fk_detalle_citadetalle` FOREIGN KEY (`idDetalle`) REFERENCES `detalle` (`idDetalle`);

--
-- Filtros para la tabla `citaempleado`
--
ALTER TABLE `citaempleado`
  ADD CONSTRAINT `citaempleado_ibfk_1` FOREIGN KEY (`idCita`) REFERENCES `cita` (`idCita`),
  ADD CONSTRAINT `citaempleado_ibfk_2` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`idEmpleado`);

--
-- Filtros para la tabla `citaservicio`
--
ALTER TABLE `citaservicio`
  ADD CONSTRAINT `citaservicio_ibfk_1` FOREIGN KEY (`idServicio`) REFERENCES `servicio` (`idServicio`),
  ADD CONSTRAINT `citaservicio_ibfk_2` FOREIGN KEY (`idCita`) REFERENCES `cita` (`idCita`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fk_empleado_persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_factura_empleado1` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  ADD CONSTRAINT `fk_factura_has_producto_factura1` FOREIGN KEY (`idFactura`) REFERENCES `factura` (`idFactura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura_has_producto_producto1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mascota`
--
ALTER TABLE `mascota`
  ADD CONSTRAINT `fk_mastcota_tipo` FOREIGN KEY (`idTipoMascota`) REFERENCES `tipomascota` (`idTipoMascota`),
  ADD CONSTRAINT `fk_persona_mascota` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_persona_usuario` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idPersona`);

--
-- Filtros para la tabla `usuariorol`
--
ALTER TABLE `usuariorol`
  ADD CONSTRAINT `fk_usuario_usuariorol` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`),
  ADD CONSTRAINT `usuariorol_ibfk_1` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
