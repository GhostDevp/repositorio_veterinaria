/*consulta para traer las citas*/
select cita.idCita, serTipo, perNombre, masNombre, ciFecha from cita inner JOIN citaservicio
on cita.idCita = citaservicio.idCita inner join servicio
on servicio.idServicio = citaservicio.idServicio inner join mascota
on mascota.idMascota = cita.idMascota inner join persona
on persona.idPersona = mascota.idPersona

/*consulta para traer a un cliente por cedula*/

select persona.idPersona, perNombre, perApellido, masNombre from persona inner join mascota
on persona.idPersona = mascota.idPersona
where perIdentificacion = ?

/*consulta para listar los detalles de una cita */
select cita.ciFecha, detalle.deDescripcion, servicio.serTipo, mascota.masNombre, persona.perNombre from cita inner JOIN citadetalle
on cita.idCita = citadetalle.idCita inner join detalle 
on citadetalle.idDetalle = detalle.idDetalle inner join citaservicio
on citaservicio.idCita = cita.idCita inner join servicio
on citaservicio.idServicio = servicio.idServicio inner join mascota 
on mascota.idMascota = cita.idMascota inner join persona
on mascota.idPersona = persona.idPersona
where mascota.idMascota = 1

/*consulta para listar las citas asignadas a empleado*/
select empleado.idEmpleado, cita.idCita, perNombre, masNombre, serTipo, ciFecha from cita inner join citaempleado
on cita.idCita = citaempleado.idCita inner join empleado 
on empleado.idEmpleado = citaempleado.idEmpleado inner join mascota
on mascota.idMascota = cita.idMascota inner join persona
on persona.idPersona = mascota.idPersona inner join citaservicio
on citaservicio.idCita = cita.idCita inner join servicio
on servicio.idServicio = citaservicio.idServicio
where empleado.idEmpleado = 2